<?php


/**
 * Some important function are here below!
 * Remove comment to start using any of these.
 */

if (!function_exists('class_name')) {

	/**
	 * If the method of request is POST return true else false.
	 * 
	 *  @return bool  */
	function class_name()
	{
		$ci = &get_instance();
		return $ci->router->fetch_class();
	}
}

if (!function_exists('get_csrf_token_name')) {
	function get_csrf_token_name()
	{
		$ci = &get_instance();
		return $ci->security->get_csrf_token_name();
	}
}

if (!function_exists('get_csrf_hash')) {
	function get_csrf_hash()
	{
		$ci = &get_instance();
		return $ci->security->get_csrf_hash();
	}
}

if (!function_exists('forms')) {
	function forms($url, $data = [])
	{

		echo form_open($url, $data);
	}
}

if (!function_exists('userdata')) {
	function userdata()
	{
		$ci = &get_instance();
		return $ci->session->userdata('data');
	}
}

if (!function_exists('flashdata')) {
	function flashdata($set, $type, $msg = false)
	{
		$ci = &get_instance();
		if ($set) {
			return $ci->session->set_flashdata($type, $msg);
		} else {
			return $ci->session->flashdata($type);
		}
	}
}

if (!function_exists('is_subdomain')) {
	function is_subdomain()
	{
		$hostname = parse_url(current_url());
		$subhost = explode('.', $hostname['host']);

		return $subhost[0];
	}
}

if (!function_exists('data_exist')) {
	function data_exist($data)
	{
		return (!empty($data) ? $data:'');
	}
}

function _input($attribut, $i = null)
{
	switch ($attribut['type']) {
		case 'text':
			$html = '<input type="text" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" required>';
			break;
		case 'checkbox':
			$html = '<input type="checkbox" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '"  required>';
			break;
		case 'radio':
			$html = '<input type="radio" class="custom-control-input" id="' . $attribut['id'] . '_' . $i . '" name="' . $attribut['name'] . '" value="' . $i . '" required>';
			break;
		case 'select':
			$html = '<select class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '" ><option value="" required>==Pilih '.$attribut['label'].'==</option></select>';
			break;
		case 'file':
			$html = '<input type="file" class="form-control ' . $attribut['class'] . '" name="' . $attribut['name'] . '" id="' . $attribut['id'] . '"  required>';
			break;
	}

	echo $html;
}

if (!function_exists('is_post')) {

	/**
	 * If the method of request is POST return true else false.
	 * 
	 *  @return bool  */
	function is_post()
	{
		return (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')  ? true : false;
	}
}

if (!function_exists('is_get')) {

	/**
	 * If the method of request is GET return true else false.
	 * 
	 *  @return bool  */
	function is_get()
	{
		return (strtoupper($_SERVER['REQUEST_METHOD']) === 'GET')  ? true : false;
	}
}

if (!function_exists('input_print')) {

	/**
	 * To print database inside a input field use this.
	 * It will escape values such as ', " or other entities.
	 * 
	 *  @return mixed  */
	function input_print($str)
	{

		return htmlentities($str);
	}
}



if (!function_exists('set_custom_header')) {
	/**
	 * It will setup custom header
	 * 
	 * @param mixed $custom_header to set css or any other thing to the header
	 * @return void It will set value only nothing will be return.
	 */
	function set_custom_header($file)
	{
		$str = '';
		$ci = &get_instance();
		$custom_header  = $ci->config->item('custom_header');

		if (empty($file)) {
			return;
		}

		if (is_array($file)) {
			if (!is_array($file) && count($file) <= 0) {
				return;
			}
			foreach ($file as $item) {
				$custom_header[] = $item;
			}
			$ci->config->set_item('custom_header', $custom_header);
		} else {
			$str = $file;
			$custom_header[] = $str;
			$ci->config->set_item('custom_header', $custom_header);
		}
	}
}

if (!function_exists('get_custom_header')) {
	/**
	 * It will get customer header if set up.
	 * 
	 *  @return void|string  */
	function get_custom_header()
	{
		$str = '';
		$ci = &get_instance();
		$custom_header  = $ci->config->item('custom_header');


		if (!is_array($custom_header)) {
			return;
		}

		foreach ($custom_header as $item) {
			$str .= $item . "
";
		}

		return $str;
	}
}

if (!function_exists('set_custom_footer')) {
	/**
	 * It will setup custom footer
	 * 
	 * @param mixed $custom_footer to set js or any other thing to the footer
	 * @return void It will set value only nothing will be return.
	 */
	function set_custom_footer($file)
	{
		$str = '';
		$ci = &get_instance();
		$custom_footer  = $ci->config->item('custom_footer');

		if (empty($file)) {
			return;
		}

		if (is_array($file)) {
			if (!is_array($file) && count($file) <= 0) {
				return;
			}
			foreach ($file as $item) {
				$custom_footer[] = $item;
			}
			$ci->config->set_item('custom_footer', $custom_footer);
		} else {
			$str = $file;
			$custom_footer[] = $str;
			$ci->config->set_item('custom_footer', $custom_footer);
		}
	}
}

if (!function_exists('get_custom_footer')) {
	/**
	 * It will get customer footer if set up.
	 * 
	 *  @return void|string  */
	function get_custom_footer()
	{
		$str = '';
		$ci = &get_instance();
		$custom_footer  = $ci->config->item('custom_footer');

		if (!is_array($custom_footer)) {
			return;
		}

		foreach ($custom_footer as $item) {
			$str .= $item . "";
		}

		return $str;
	}
}



/* End of file view_helper.php and path \application\helpers\view_helper.php */
