<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dbsetup
{
	protected $CI;
	protected $HostDB;
	protected $UserDB;
	protected $PassDB;
	protected $NameDB;
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->HostDB = 'localhost'; // ubah sesuai host database
		$this->UserDB = 'root'; 	// ubah sesuai set user db and
		$this->PassDB = '';			// ubah sesusai set password anda
		$this->NameDB = 'db_dispan'; // ubah sesuai set nama db anda
		$init = array();
		$init['file'] = 'database.php';
		$init['variable_name'] = 'db';
		$this->CI->load->library('ConfigWriter', $init, 'configwriter');
	}

	public function GetSubDomain()
	{
		$hostname = parse_url(current_url());
		$subhost = explode('.', $hostname['host']);

		return $subhost[0];
	}


	public function configure()
	{
		$this->CI->configwriter->write(array('default', 'hostname'), $this->HostDB);
		$this->CI->configwriter->write(array('default', 'username'), $this->UserDB);
		$this->CI->configwriter->write(array('default', 'password'), $this->PassDB);
		$this->CI->configwriter->write(array('default', 'database'), $this->NameDB);
	}
}


/* End of file Dbsetupt.php and path \application\libraries\Dbsetupt.php */
