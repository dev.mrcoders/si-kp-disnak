<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends MY_Controller
{
	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		//$this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->user = userdata()->nip;
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}
	public function index($page = null)
	{
		$this->data['title']='User Account';
		$this->view('myprofile',$this->data);
	}

	public function UpdateProfile()
	{
		$PostProfile = [
			'nip'=>$this->input->post('nip'),
			'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'telepon'=>$this->input->post('phone'),
		];
		$this->db->where('nip', $this->user);
		$result = $this->db->update('tb_user', $PostProfile);
		
		if($result){
			$this->json=[
				'success'=>true,
				'message'=>"Profile Anda Berhasil Di Update, Silahkan Login Ulang Untuk menerapkan Perubahan",
				'redirect'=>'islogout'
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
	
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function UpdatePassword()
	{
		$PostProfile = [
			'password'=>md5($this->input->post('re_password')),
		];
		$this->db->where('nip', $this->user);
		$result = $this->db->update('tb_user', $PostProfile);
		
		if($result){
			$this->json=[
				'success'=>true,
				'message'=>"Password Anda Berhasil Di Update, Silahkan Login Ulang Untuk menerapkan Perubahan, dan login menggunakan password baru anda",
				'redirect'=>'islogout'
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
	
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}
}
