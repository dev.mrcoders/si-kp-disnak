<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditi extends MY_Controller {
	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->user = userdata()->nip;
		$this->load->model('M_jeniskomoditi', 'jkomoditi');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Jenis Ternak';
		$this->view('komoditi.data', $this->data);

	}

	public function DataTables()
	{
		$list = $this->jkomoditi->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->nama_ternak;
			$row[] = $r->faktor_x;
			$row[] = '<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
			<button class="btn waves-effect waves-light btn-outline-warning edit" data-id="'.$r->id_jenis_ternak.'"><i class="mdi mdi-pencil-box-outline" ></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger delete" data-id="'.$r->id_jenis_ternak.'"><i class="mdi mdi-delete" ></i> hapus</button>
			</div>';

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jkomoditi->count_all(),
			"recordsFiltered" => $this->jkomoditi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataById()
	{

		$Data = $this->jkomoditi->GetDataById();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id=null)
	{
		foreach($this->input->post('jenis') as $key => $value) {
			$FormData []=[
				'nama_ternak'=>$value,
				'faktor_x'=>$this->input->post('produksi')[$key]
				
			];
		}

		if ($id == null) {
			$result = $this->jkomoditi->Created($FormData);
		}else{
			foreach($FormData as $key=>$v){
				$FormData[$key]['id_jenis_ternak']=$id;
			}
			$result = $this->jkomoditi->Updated($FormData);
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{

		$result = $this->jkomoditi->Deleted($this->input->post('id'));
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->json['status'] = $result;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

}

/* End of file Lampres.php */
	/* Location: ./application/modules/disnak/controllers/Lampres.php */
