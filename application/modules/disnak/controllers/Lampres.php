<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lampres extends MY_Controller
{

	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->user = userdata()->nip;
		$this->load->model('M_lampres', 'dlampres');
		$this->load->model('M_jeniskomoditi', 'jkomoditi');
		$this->load->model('M_report', 'report');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index($page = false)
	{
		$this->data['title'] = 'Data Master Produksi';
		switch ($page) {
			case 'kabupaten':
			$this->view('lampres.kabupaten', $this->data);
			break;
			case 'kecamatan':
			$this->view('lampres.kecamatan', $this->data);
			break;
			default:
			$data['page'] = 'index';
			$this->view('index', $this->data);
			break;
		}
		
	}

	public function DataTables()
	{

		$list = $this->dlampres->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();
			$row['bln_thn'] = date('M Y', strtotime($r->created));
			$row['kecamatan'] = $r->nama_kecamatan;
			$JenisTernak = $this->db->get('tb_jenis_ternak')->result();
			$Ternak = [];
			foreach ($JenisTernak as $key => $v) {
				$DataProd = $this->db->get_where('tb_ternak_transaksi', ['id_data_produksi' => $r->id_produksi, 'id_jenis_ternak' => $v->id_jenis_ternak])->row();
				$row2 = [];
				$row2['id_transaksi'] = ($DataProd->populasi != null ? $DataProd->id_transaksi : '0');
				$row2['populasi'] = ($DataProd->populasi != null ? $DataProd->populasi : '0');
				$row2['ternak'] = $v->nama_ternak;
				$row2['pemotongan'] = ($DataProd->pemotongan_ternak != null ? $DataProd->pemotongan_ternak : '0');
				$row2['produksi_daging'] = ($DataProd->produksi_daging != null ? $DataProd->produksi_daging : '0');
				$row2['pemasukan'] = ($DataProd->pemasukan != null ? $DataProd->pemasukan : '0');
				$row2['pengeluaran'] = ($DataProd->pengeluaran != null ? $DataProd->pengeluaran : '0');
				$row2['kelahiran'] = ($DataProd->kelahiran != null ? $DataProd->kelahiran : '0');
				$row2['kematian'] = ($DataProd->kematian != null ? $DataProd->kematian : '0');
				$row2['ket'] = ($DataProd->keterangan != null ? $DataProd->keterangan : '0');;
				$Ternak[] = $row2;
			}
			$row['produksi'] = $Ternak;
			$row['id_produksi'] = $r->id_produksi;
			$row['button'] = '<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
			<button type="button" class="btn waves-effect waves-light btn-outline-info view"><i class="mdi mdi-eye"></i> Lihat</button>
			<button type="button" class="btn waves-effect waves-light btn-outline-warning edit" data-id="' . $r->id_produksi . '"><i class="mdi mdi-pencil-box"></i>Ubah</button>
			<button type="button" class="btn waves-effect waves-light btn-outline-danger delete" data-id="' . $r->id_produksi . '"><i class="mdi mdi-delete"></i>Hapus</button>
			</div>';
			$data[] = $row;
		}
		
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->dlampres->count_all(),
			"recordsFiltered" => $this->dlampres->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;

		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataTablesForm()
	{
		$list = $this->jkomoditi->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		$Readonly = ($this->input->post('edits') == 'true' ? '' : 'readonly');
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $r->nama_ternak . '<input type="hidden" name="id_jenis_ternak[]" value="' . $r->id_jenis_ternak . '" readonly class="form-control"/>';
			$row[] = '<input type="text" name="populasi[]" value="' . $r->populasi . '" class="form-control number" ' . $Readonly . ' />';
			$row[] = '<input type="text" name="pemotongan_ternak[]" value="' . $r->pemotongan_ternak . '" class="form-control number" onkeyup="HasilProd(this,' . $r->faktor_x . ',' . $r->id_jenis_ternak . ')" ' . $Readonly . '/>';
			$row[] = '<input type="text" name="produksi_daging[]" value="' . $r->produksi_daging . '" class="form-control prods" readonly id="prods_' . $r->id_jenis_ternak . '" />';
			$row[] = '<input type="text" name="pemasukan[]" value="' . $r->pemasukan . '" class="form-control number" ' . $Readonly . ' />';
			$row[] = '<input type="text" name="pengeluaran[]" value="' . $r->pengeluaran . '" class="form-control number" ' . $Readonly . ' />';
			$row[] = '<input type="text" name="kelahiran[]" value="' . $r->kelahiran . '" class="form-control number" ' . $Readonly . ' />';
			$row[] = '<input type="text" name="kematian[]" value="' . $r->kematian . '" class="form-control number" ' . $Readonly . ' />';
			$row[] = '<input type="text" name="keterangan[]" value="' . $r->keterangan . '" class="form-control " ' . $Readonly . '/>';


			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jkomoditi->count_all(),
			"recordsFiltered" => $this->jkomoditi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}


	public function DatatablesReport()
	{
		$fieldsMaster = $this->report->ListColumnName();
		$list = $list = $this->report->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();
			$row['no'] = $no;
			$row['jenis_ternak'] = $r['nama_ternak'];
			foreach ($fieldsMaster as $key => $field) {
				if($key > 2 && $key < 10){
					$row[$field->name] = $r[$field->name];
				}
			}
			$row['keterangan'] = $r['keterangan']; 	
			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->report->count_all(),
			"recordsFiltered" => $this->report->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}



	public function CheckingData()
	{
		$post = $this->input->post();
		$fieldsMaster = $this->db->field_data('tb_ternak_produksi');
		foreach ($fieldsMaster as $key => $vl) {
			if ($key != 0) {
				if ($vl->name == 'created') {
					$post['created'] = $this->input->post('tahun') . '-' . $this->input->post('bulan');
				}
				$Master[$vl->name] = $post['' . $vl->name . ''];
			}
		}

		$IsDataExist = $this->db->get_where('tb_ternak_produksi', $Master);
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_ternak_transaksi', ['id_data_produksi' => $IsDataExist->row()->id_produksi]);
			if ($DataTrx->num_rows() > 0) {
				$this->json = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Sudah Ada, Silahkan Ganti Tahun Bulan Dan Kecamatan Anda',
					'data' => $IsDataExist->row()->id_produksi
				];
			} else {
				$this->json = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Hanya Data Master Tidak Ada Data Transaksi!!<br>Anda Dapat Menambahkan Data Transaksi',
					'data' => $IsDataExist->row()->id_produksi
				];
			}
		} else {
			$this->json = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Tidak Ada Data Yang Ditemukan!! <br>Anda Dapat Menambahkan Data',
				'data' => $IsDataExist->row()->id_produksi
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataById()
	{
		$Data = $this->dlampres->GetDataById();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{

		$post = $this->input->post();
		if ($id == null) {
			$fieldsMaster = $this->db->field_data('tb_ternak_produksi');
			foreach ($fieldsMaster as $key => $vl) {
				if ($key != 0) {
					if ($vl->name == 'created') {
						$post['created'] = $this->input->post('tahun') . '-' . $this->input->post('bulan');
					}
					$Master[$vl->name] = $post['' . $vl->name . ''];
				}
			}

			$this->dlampres->Created($Master, false);
			$id_data_prod = $this->db->insert_id();
		} else {
			$id_data_prod = $id;
		}

		$fields = $this->db->field_data('tb_ternak_transaksi');
		foreach ($post['id_jenis_ternak'] as $keys => $value) {
			$post['id_data_produksi'][$keys] = $id_data_prod;
			$FormData = array();
			foreach ($fields as $key => $name) {
				if ($key != 0) {
					if ($name == 'id_jenis_ternak') {
						$post['id_jenis_ternak'] = $value;
					}
					$FormData[$name->name] = $post[$name->name][$keys];
				}
			}

			$Data[] = $FormData;
		}

		$this->dlampres->CreatedTrx($Data);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Update()
	{
		$post = $this->input->post();
		$id_data_prod = $this->input->post('id_produksi');
		$fields = $this->db->field_data('tb_ternak_transaksi');
		foreach ($post['id_jenis_ternak'] as $keys => $value) {
			$post['id_data_produksi'][$keys] = $id_data_prod;
			$FormData = array();
			foreach ($fields as $key => $name) {
				if ($key != 0) {
					if ($name == 'id_jenis_ternak') {
						$post['id_jenis_ternak'] = $value;
					}
					$FormData[$name->name] = $post[$name->name][$keys];
				}
			}

			$Data[] = $FormData;
		}

		$result = $this->dlampres->Updated($Data, $id_data_prod);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$result = $this->dlampres->Deleted($this->input->post('id'), $this->input->post('data'));
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->json['status'] = $result;
		$this->json['post'] = [$this->input->post('id'), $this->input->post('data')];
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}
}

/* End of file Lampres.php */
/* Location: ./application/modules/disnak/controllers/Lampres.php */
