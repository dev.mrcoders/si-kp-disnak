<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lampres extends CI_Model {

     //set nama tabel yang akan kita tampilkan datanya
    var $table = array('tb_ternak_transaksi','tb_ternak_produksi');

    var $id_keys = 'id_produksi';
     //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order = array(null, 'nama_kecamatan','created');

    var $column_search = array('nama_kecamatan','created');
     // default order 
    var $order = array('id_produksi' => 'desc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($reports=false)
    {
        if ($this->input->post('kecamatan')) {
            $this->db->where('tb_kecamatan.kd_kecamatan', $this->input->post('kecamatan'));
        }
        if ($this->input->post('tahun')) {
            $this->db->where('SUBSTRING('.$this->table[1].'.created,1,4) =', $this->input->post('tahun'));
        }
        if ($this->input->post('bulan')) {
            $this->db->where('SUBSTRING('.$this->table[1].'.created,6,2) =', $this->input->post('bulan'));
        }
        $this->db->join('tb_kecamatan', 'tb_kecamatan.kd_kecamatan='.$this->table[1].'.kd_kecamatan', 'left');
        $this->db->from($this->table[1]);

        $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                $this->db->group_end();
            }
            $i++;
        }

   // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table[1]);
        return $this->db->count_all_results();
    }

    public function Created($data,$batch)
    {
        if ($batch) {
            return $this->db->insert_batch($this->table[1], $data); 
        }else{
            return $this->db->insert($this->table[1], $data);
        }

    }

    public function CreatedTrx($data)
    {

        return $this->db->insert_batch('tb_ternak_transaksi', $data); 


    }

    public function Updated($data,$id)
    {
        $this->db->where('id_data_produksi', $id);
        return $this->db->update_batch('tb_ternak_transaksi', $data, 'id_jenis_ternak');
    }

    public function GetDataById()
    {
        $this->db->select($this->table[1].'.*,a.nama_kecamatan');
        $this->db->join('tb_kecamatan a', 'a.kd_kecamatan='.$this->table[1].'.kd_kecamatan', 'inner');
        if ($this->input->get('id')) {
            $this->db->where($this->id_keys, $this->input->get('id'));
            $hasil = $this->db->get($this->table[1])->row();
        }else{
            $hasil = $this->db->get($this->table[1])->result();
        }

        return $hasil;
    }

    public function Deleted($id,$type)
    {
        if ($type == 'master') {
            $query = $this->db->where('id_produksi', $id)->delete($this->table[1]);
            $query = $this->db->where_in('id_data_produksi', [$id])->delete($this->table[0]);
        }else{
			$this->db->where('id_data_produksi', $id);
            $Hasil = $this->db->get($this->table[0]);
			foreach($Hasil->result() as $i=>$v){
				$field[]=[
					'id_transaksi'=>$v->id_transaksi,
					'populasi'=>0,
					'pemotongan_ternak'=>0,
					'produksi_daging'=>0,
					'pemasukan'=>0,
					'pengeluaran'=>0,
					'kelahiran'=>0,
					'kematian'=>0,
					'keterangan'=>'',
				];
			}
			$this->db->update_batch($this->table[0], $field,'id_transaksi');
			
        }
        return $query;
    }

    public function ListColumnName()
    {
        return $this->db->field_data($this->table[0]);
    }

}

/* End of file M_jenisikan.php */
     /* Location: ./application/modules/diskan/models/M_jenisikan.php */
