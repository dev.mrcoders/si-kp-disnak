<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_report extends CI_Model {

     //set nama tabel yang akan kita tampilkan datanya
    var $table = array('tb_ternak_transaksi a','tb_ternak_produksi b');

    var $id_keys = 'id_produksi';
     //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order = array(null, 'c.nama_ternak','a.populasi');

    var $column_search = array('c.nama_ternak','a.populasi');
     // default order 
    var $order = array('a.id_jenis_ternak' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $fieldsMaster = $this->db->field_data('tb_ternak_transaksi');
        foreach ($fieldsMaster as $key => $field) {
            if($key > 2 && $key < 10){
                $this->db->select('sum(a.'.$field->name.')as '.$field->name);
            }
        }

        $this->db->from($this->table[0]);

        $this->db->select('a.keterangan');
        $this->db->select('c.*');
        $this->db->join('tb_jenis_ternak c', 'a.id_jenis_ternak=c.id_jenis_ternak', 'inner');
        $this->db->join('tb_ternak_produksi b', 'b.id_produksi=a.id_data_produksi', 'inner');
        if ($this->input->post('tahun')) {
            $this->db->where('SUBSTRING(b.created,1,4) =', $this->input->post('tahun'));
        }
        if ($this->input->post('bulan')) {
            $this->db->where('SUBSTRING(b.created,6,2) =', $this->input->post('bulan'));
        }
        
        
        $this->db->group_by('a.id_jenis_ternak');
        $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }


    public function ListColumnName()
    {
        return $this->db->field_data('tb_ternak_transaksi');
    }

}

/* End of file M_jenisikan.php */
/* Location: ./application/modules/diskan/models/M_jenisikan.php */