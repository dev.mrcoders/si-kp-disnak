@extends('layouts.index')
@section('content')
<style>
    table.dataTable thead th {
        white-space: nowrap;
        font-size: 12px;
        text-align: center;
    }


    table.dataTable tbody td {
        white-space: nowrap;
        font-size: 11px;
        text-align: center;
    }

    @media (max-width: 575.98px) {
        .modal-fullscreen {
            padding: 0 !important;
        }

        .modal-fullscreen .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 767.98px) {
        .modal-fullscreen-sm {
            padding: 0 !important;
        }

        .modal-fullscreen-sm .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-sm .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-sm .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 991.98px) {
        .modal-fullscreen-md {
            padding: 0 !important;
        }

        .modal-fullscreen-md .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-md .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-md .modal-body {
            overflow-y: auto;
        }
    }

    @media (max-width: 1199.98px) {
        .modal-fullscreen-lg {
            padding: 0 !important;
        }

        .modal-fullscreen-lg .modal-dialog {
            width: 100%;
            max-width: none;
            height: 100%;
            margin: 0;
        }

        .modal-fullscreen-lg .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }

        .modal-fullscreen-lg .modal-body {
            overflow-y: auto;
        }
    }

    .modal-fullscreen-xl {
        padding: 0 !important;
    }

    .modal-fullscreen-xl .modal-dialog {
        width: 100%;
        max-width: none;
        height: 100%;
        margin: 0;
    }

    .modal-fullscreen-xl .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0;
    }

    .modal-fullscreen-xl .modal-body {
        overflow-y: auto;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <button type="button" class="btn waves-effect waves-light btn-primary tab-page" data-page="kecamatan">Data
            Lampres Kecamatan</button>
            <button type="button" class="btn waves-effect waves-light btn-secondary tab-page" data-page="kabupaten">Data
            Lampres Kabupaten</button>
        </div>
        <div class="col-lg-12" id="page-data">

        </div>
    </div>
</div>

<script>
    const monthNames = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agusturs", "September",
        "Oktober", "November", "Desember"
        ];
    const years = [];
    const months = [];
    const currentYear = (new Date()).getFullYear();
    const currentMonth = new Date().getMonth();
    $(function() {
        let sess = $.session.get('tabs');
        let pages = (sess == undefined ? 'kecamatan' : sess)
        $('#page-data').load(base_url + 'disnak/data-lampres/index/' + pages, function() {

        });

        let page_tab = $('.tab-page');
        $.each(page_tab, function(index, item) {

            if ($(item).data('page') == pages) {

                $(this).removeClass('btn-secondary').addClass('btn-primary');
            } else {
                $('button[data-page="' + $(item).data('page') + '"]').removeClass('btn-primary')
                .addClass('btn-secondary');
            }
        });

        $('.tab-page').on('click', function(event) {
            event.preventDefault();
            let page = $(this).data('page');

            $('#page-data').load(base_url + 'disnak/data-lampres/index/' + page);

            $.each(page_tab, function(index, item) {
                if (page == $(item).data('page') || $(item).data('page') == page) {
                    $.session.set('tabs', page);
                    $(this).removeClass('btn-secondary').addClass('btn-primary');
                } else {
                    $('button[data-page="' + $(item).data('page') + '"]').removeClass(
                        'btn-primary').addClass('btn-secondary');
                }
            });
        });
    });
</script>
@endsection
