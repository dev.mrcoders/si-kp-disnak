@extends('layouts.index')
@section('content')
    <style>
        tr.group,
        tr.group:hover {
            background-color: #ddd !important;
        }

        table.dataTable thead th {
            white-space: nowrap
        }

        td.details-control {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.details td.details-control {
            background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body ">
                        <div class="row mb-4">
                            <div class="col-sm-3">
                                <h4 class="card-title">Data Jenis Ternak</h4>
                            </div>
                            <div class="col-sm-9 text-right">
                                <button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i
                                        class="mdi mdi-plus"></i> Tambah Data</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table nowrap" id="tb-data" width="100%">
                                <thead class="bg-info text-white">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Ternak</th>
                                        <th>Porduksi/ekor</th>
                                        <th>Act</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Data Jenis Ternak</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ forms(base_url() . 'disnak/data-ternak/save', ['id' => 'form-komoditi']) }}
                <input type="hidden" name="data_jenis" value="komoditi">
                <div class="modal-body" id="fields-komoditi">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label for="">Nama Ternak</label>
                                <input type="text" class="form-control" id="komoditi" name="jenis[]"
                                    placeholder="Nama Ternak" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="">Produksi Perekor</label>
                                <input type="text" class="form-control" id="produksi" name="produksi[]"
                                    placeholder="Produksi Perekor" required>
                            </div>
                        </div>
                        <div class="col-sm-2" id="btn-row-add">
                            <div class="form-group pt-4">
                                <button class="btn btn-success mt-1" type="button" onclick="education_fields();"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            ajaxcsrf();
            Datatables();

            function Datatables() {
                $('#tb-data').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "disnak/data-ternak/tables",
                        "type": "POST",
                        "data": {
                            token: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    }
                });
            }

            $('.add').on('click', function(event) {
                event.preventDefault();
                $('form#form-komoditi').attr('action', base_url + 'disnak/data-ternak/save');
                $('form#form-komoditi')[0].reset();
                $('#add-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            });

            $('tbody').on('click', '.edit', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                $.ajax({
                    url: base_url + 'disnak/data-ternak/databyid',
                    type: 'get',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        $('form#form-komoditi').attr('action', base_url +
                            'disnak/data-ternak/save/' + id);
                        $('#komoditi').val(data.nama_ternak);
                        $('#produksi').val(data.faktor_x);
                        $('#btn-row-add').hide('fast');
                        $('#add-modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                    }
                });

            });

            $('tbody').on('click', '.delete', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: base_url + 'disnak/data-ternak/delete',
                            type: 'POST',
                            data: {
                                id: id,
                                csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
                            },
                            success: function(data) {
                                $('meta[name="csrf-token"]').attr("content", data
                                    .csrf_param);
                                Datatables();
                                if (data.status) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Data behasil Di Hapus.',
                                        'success'
                                    )
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'data Tidak Bisa Di hapus, Data Digunakan Pada data master',
                                    })
                                }


                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });

                    }
                })

            });

            $.extend($.validator.prototype, {
                checkForm: function() {
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[
                            i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(
                                elements[i].name).length > 1) {
                            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                }
            });

            $("form#form-komoditi").validate({
                rules: {
                    "jenis[]": {
                        required: true
                    },
                    "produksi[]": {
                        required: true
                    },

                },
                messages: {
                    "jenis[]": {
                        required: "Tidak Boleh Kosong"
                    },
                    "produksi[]": {
                        required: "Tidak Boeh kosong / harus bernilai 0"
                    },

                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: Saved,
            });

            function Saved() {
                $.ajax({
                    url: $("form#form-komoditi").attr('action'),
                    type: 'POST',
                    data: $("form#form-komoditi").serialize() + "&csrf_hash_name=" + $(
                        'meta[name="csrf-token"]').attr("content"),
                    success: function(data, textStatus, xhr) {
                        $('meta[name="csrf-token"]').attr("content", data.csrf_param);
                        if (xhr.status == 200) {
                            Success();
                            $('#add-modal').modal('hide');
                            Datatables();
                            console.log(data);
                        }
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }

        });

        var room = 1;

        function education_fields() {

            room++;
            var objTo = document.getElementById('fields-komoditi')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML = '<div class="col-sm-7">' +
                '<div class="form-group">' +
                '<label for="">Nama Ternak</label>' +
                '<input type="text" class="form-control" id="Schoolname" name="jenis[]" placeholder="Nama Ternak" required>' +
                '</div></div>' +
                '<div class="col-sm-3">' +
                '<div class="form-group">' +
                '<label for="">Produksi Perekor</label>' +
                '<input type="text" class="form-control" id="produksi" name="produksi[]" placeholder="Produksi Perekor" required>' +
                '</div></div>' +
                '<div class="col-sm-2"> ' +
                '<div class="form-group pt-4"> ' +
                '<button class="btn btn-danger mt-1" type="button" onclick="remove_education_fields(' + room +
                ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div></div></div>';

            objTo.appendChild(divtest)
        }

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>
@endsection
