{{ forms('', ['id' => 'form-lampres-edit']) }}
<div class="row" id="">
    <div class="col-sm-4">
        <div class="form-group">
            <select name="tahun" id="tahun" class="form-control">
                <option value="">==Pilih Tahun==</option>

            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <select name="bulan" id="bulan" class="form-control" onchange="CheckingData()">
                <option value="">==Pilih Bulan==</option>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <select name="kd_kecamatan" id="kd_kecamatan" class="form-control kecamatan" onchange="CheckingData()"
                style="width: 100%;">
            </select>
        </div>
    </div>
    <input type="hidden" name="id_produksi" value="" id="id_produksi-edit" class="form-control id_produksi_edit">
</div>
<table class="table " width="100%" id="tb-form-produksi-edit">
    <thead class="bg-info text-white">
        <?php
        $Headers = ['Jenis Ternak', 'Populasi Ternak (ekor)', 'Pemotongan Ternak (ekor)', 'Produksi Daging (kg)', 'Pemasukan (ekor)', 'Pengeluaran (ekor)', 'Kelahiran (ekor)', 'Kematian (ekor)', 'Keterangan'];
        ?>
        <tr>
            <?php foreach ($Headers as $key => $values) : ?>
            <th><?= $values ?></th>
            <?php endforeach ?>

        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
</form>
