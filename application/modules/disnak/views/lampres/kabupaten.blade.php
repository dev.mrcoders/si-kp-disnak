<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Bulan</label>
                    <select name="" id="fl_bulan" class="form-control">
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Tahun</label>
                    <select name="" id="fl_tahun" class="form-control">
                    </select>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group pt-4">
                    <button class="btn btn-sm btn-primary mt-2" id="filterData"><i class="mdi mdi-filter"></i>
                        Filter</button>
                    <button class="btn btn-sm btn-dark mt-2" id="resetFilter"><i class="mdi mdi-refresh"></i>
                        Reset</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body ">
        <div class="row mb-4">
            <div class="col-sm-12 ">
                <h4 class="card-title text-center font-weight-bold text-uppercase">laporan petugas pendataan peternakan
                    kabupaten kampar</h4>
                <h4 class="card-title text-uppercase text-center font-weight-bold">Bulan <span id="txt-bln"></span>
                    Tahun <span id="txt-thn"></span></h4>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table" id="tb-data" width="100%">
                <thead class="bg-info text-white">
                    <tr>
                        <th>No</th>
                        <th>Jenis Ternak </th>
                        <th>Populasi Ternak (ekor)</th>
                        <th>Pemotongan Ternak (ekor)</th>
                        <th>Produksi Daging (kg)</th>
                        <th>Pemasukan (ekor)</th>
                        <th>Pengeluaran (ekor)</th>
                        <th>Kelahiran (ekor)</th>
                        <th>Kematian (ekor)</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    var nowYear = (new Date()).getFullYear();
    var nowMonth = new Date().getMonth();
    var bln = ("0" + (nowMonth + 1)).slice(-2);
    $(function() {
        $('#txt-bln').text(monthNames[nowMonth]);
        $('#txt-thn').text(nowYear);
        Datatables();

        function Datatables() {
            table = $('#tb-data').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                scrollY: '100vh',
                scrollCollapse: true,
                paging: false,
                stateSave: true,
                destroy: true,
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": base_url + "disnak/data-lampres/tables-report",
                    "type": "POST",
                    "data": {
                        token: $('meta[name="csrf-token"]').attr("content"),
                    },
                    "data": function(data) {
                        data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                        data.tahun = $('#fl_tahun').val();
                        data.bulan = $('#fl_bulan').val();
                    },
                    "dataSrc": function(response) {
                        $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                        return response.data;
                    },
                },
                columns: [{
                        data: 'no'
                    },
                    {
                        data: 'jenis_ternak'
                    },
                    {
                        data: 'populasi',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'pemotongan_ternak',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'produksi_daging',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'pemasukan',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'pengeluaran',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'kelahiran',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'kematian',
                        render: function(data) {
                            return $.number(data, 0)
                        }
                    },
                    {
                        data: 'keterangan'
                    },
                ],

            });
        }

        $('#filterData').on('click', function(event) {
            event.preventDefault();
            Datatables();
            $('#txt-bln').text(monthNames[$('#fl_bulan').val().replace(/^0+/, '') - 1]);
            $('#txt-thn').text($('#fl_tahun').val());
        });

        $('#resetFilter').on('click', function(event) {
            event.preventDefault();

            $('#fl_tahun, #fl_bulan').val('');
            Datatables();
        });

        for (var i = currentYear; i >= 2015; i--) {
            years.push({
                'id': i,
                'text': i
            });
        }

        for (var month = 0; month <= 11; month++) {
            bln = ("0" + (month + 1)).slice(-2);
            months.push({
                'id': bln,
                'text': monthNames[month]
            });
        }

        $('#fl_tahun').select2({
            width: '100%',
            theme: 'bootstrap4',
            allowClear: true,
            placeholder: '==Pilih tahun==',
            data: years
        });

        $('#fl_bulan').select2({
            width: '100%',
            theme: 'bootstrap4',
            allowClear: true,
            placeholder: '==Pilih Bulan==',
            data: months
        });






    });
</script>
