
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Tahun</label>
                    <select name="" id="ddlYears" class="form-control">
                        <option value="">==Pilih Tahun==</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Bulan</label>
                    <select name="" id="ddlMonth" class="form-control">
                        <option value="">==Pilih Bulan==</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Kecamatan</label>
                    <select name="" id="kecamatan"></select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group pt-4">
                    <button class="btn btn-sm btn-primary mt-2" id="filterData"><i class="mdi mdi-filter"></i>
                    Filter</button>
                    <button class="btn btn-sm btn-dark mt-2" id="resetFilter"><i class="mdi mdi-refresh"></i>
                    Reset</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body ">
        <div class="row mb-4">
            <div class="col-sm-3">
                <h4 class="card-title">Data Distribusi Benih</h4>
            </div>
            <div class="col-sm-9 text-right">
                <button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"> <i
                    class="mdi mdi-plus"></i> Tambah Data</button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table" id="tb-data" width="100%">
                    <thead class="bg-info text-white">
                        <tr>
                            <th>No</th>
                            <th>Bulan Tahun</th>
                            <th>Kecamatan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="modal modal-fullscreen-xl" id="add-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        @include('lampres.form.c_kecamatan')
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" id="resetForm">Reset Input</button>
                    <button type="submit" class="btn btn-primary saved" data-aksi="">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        var table;
        $(function() {
            ajaxcsrf();
            Datatables()
            for (var i = currentYear; i >= 2015; i--) {
                years.push({
                    'id': i,
                    'text': i
                });
            }

            for (var month = 0; month <= 11; month++) {
                bln = ("0" + (month + 1)).slice(-2);
                months.push({
                    'id': bln,
                    'text': monthNames[month]
                });
            }

            $('#ddlYears, #tahun').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih tahun==',
                data: years
            });

            $('#ddlMonth, #bulan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih Bulan==',
                data: months
            });

            $('#kecamatan, #kd_kecamatan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama kecamatan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'select2-kecamatan',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });

            $('#filterData').on('click', function(event) {
                event.preventDefault();
                Datatables();
            });

            $('#resetFilter').on('click', function(event) {
                event.preventDefault();
                $("#ddlYears, #ddlMonth, #kecamatan").val(null).trigger("change");
                Datatables();

            });

            $('#resetForm').on('click', function(event) {
                event.preventDefault();

                $('#tb-form-produksi :input').val('');

            });

            function Datatables() {
                table = $('#tb-data').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "disnak/data-lampres/tables",
                        "type": "POST",
                        "data": {
                            token: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                            data.kecamatan = $('#kecamtan').val();
                            data.tahun = $('#ddlYears').val();
                            data.bulan = $('#ddlMonth').val();
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    },
                    columns: [{
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        data: 'bln_thn'
                    },
                    {
                        data: 'kecamatan'
                    },
                    {
                        data: 'button'
                    }
                    ],

                });
            }

            function TablesForm(edit = false) {
                $('#tb-form-produksi').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    pageLength: 50,
                    "dom": '<"dt-buttons"Bfli>rtp',
                    "searching": false,
                    "paging": false,
                    "autoWidth": false,
                    "fixedHeader": true,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "disnak/data-lampres/tables-form",
                        "type": "POST",
                        "data": {
                            token: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                            data.id_produksi = $('#id_produksi').val();
                            data.edits = edit;
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    }
                });
            }

            var detailRows = [];
            $('#tb-data tbody').on('click', '.view', function() {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var idx = detailRows.indexOf(tr.attr('id'));
                if (row.child.isShown()) {
                    $(this).html('<i class="mdi mdi-eye"></i> Lihat');
                    tr.removeClass('details');
                    row.child.hide();
                    detailRows.splice(idx, 1);
                } else {
                    $(this).html('<i class="mdi mdi-eye-off"></i> Tutup');
                    tr.addClass('details');
                    row.child(format(row.data())).show();
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }
                }
            });

            function format(d) {
                let table = '<table class="table tb-details table-hover" width="100%">';
                table += '<thead class="bg-warning text-white">' +
                '<tr><th>Jenis Ternak</th>' +
                '<th>Populasi Ternak (ekor)</th>' +
                '<th>Pemotongan Ternak (ekor)</th>' +
                '<th>Produksi Daging (kg)</th>' +
                '<th>Pemasukan (ekor)</th>' +
                '<th>Pengeluaran (ekor)</th>' +
                '<th>Kelahiran (ekor)</th>' +
                '<th>Kematian (ekor)</th>' +
                '<th>Keterangan </th>' +
                '</tr></thead><tbody>';
                $.each(d.produksi, function(index, val) {

                    table += '<tr>' +
                    '<td>' + val.ternak + '</td>' +
                    '<td >' + $.number(val.populasi, 0) + '</td>' +
                    '<td>' + $.number(val.pemotongan, 0) + '</td>' +
                    '<td>' + $.number(val.produksi_daging, 0) + '</td>' +
                    '<td>' + $.number(val.pemasukan, 0) + '</td>' +
                    '<td>' + $.number(val.pengeluaran, 0) + '</td>' +
                    '<td>' + $.number(val.kelahiran, 0) + '</td>' +
                    '<td>' + $.number(val.kematian, 0) + '</td>' +
                    '<td>' + val.ket + '</td>' +
                    '</tr>';
                });
                table += '</tbody></table>';
                return table;
            }

            $('#tb-form-produksi').on('keyup', '.number', function(event) {
                if (event.which >= 37 && event.which <= 40) return;
                $(this).val($(this).val());
                $(this).number(true, 0, ',', '.');

            });

            $('.add').on('click', function(event) {
                event.preventDefault();
                TablesForm();
                $('form#form-lampres').attr('action', base_url + 'disnak/data-lampres/save');
                $('form#form-lampres').attr('data-forms', 'saved');
                $('button.saved').attr('data-aksi', 'saved');
                $('#id_produksi').val('');
                var OpsKec = new Option('-Pilih Kecamatan-', '', true, true);
                $('#kd_kecamatan').append(OpsKec).prop("disabled", false);
                $('#tahun, #bulan').val('').prop("disabled", false);
                $('#add-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            });

            $('tbody').on('click', '.edit', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                $('.id_produksi').val(id);
                $.ajax({
                    url: base_url + 'disnak/data-lampres/databyid',
                    type: 'get',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        date = data.created.split('-');
                        var OpsKec = new Option(data.nama_kecamatan, data.kd_kecamatan, true,
                            true);
                        var OpsThn = new Option(date[0], date[0], true, true);
                        var OpsBln = new Option(monthNames[parseInt(date[1], 10) - 1], date[1],
                            true, true);
                        $('#kd_kecamatan').append(OpsKec).prop("disabled", true);
                        $('#tahun').append(OpsThn).prop("disabled", true);
                        $('#bulan').append(OpsBln).prop("disabled", true);;
                        $('form#form-lampres').attr('action', base_url +
                            'disnak/data-lampres/update/' + id);
                        $('form#form-lampres').attr('data-forms', 'update');
                        $('.saved').attr('data-aksi', 'update');
                        $('tbody input').attr('readonly', false);
                        $('.prods').attr('readonly', true);
                        TablesForm(true);
                        $('#add-modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                    }
                });

            });

            $('button.saved').on('click', function() {
                let aksi = $(this).attr('data-aksi');
                if (aksi == 'saved') {
                    let valid = $('#form-lampres').valid();
                    if (valid) {
                        Saved();
                    }
                } else {
                    Saved();
                }


            });

            $('tbody').on('click', '.delete', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                let info =
                '<small>Hapus Data Master : Menghapus Seluruh Data<br>Hapus Data Transaski: Hanya Menghapus Data Transaksi Saja</small>';
                Swal.fire({
                    title: 'Apa Anda Yakin?',
                    html: info,
                    icon: 'warning',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Hapus Data',
                    denyButtonText: 'Kosong Kan data',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: base_url + 'disnak/data-lampres/delete',
                            type: 'POST',
                            data: {
                                id: id,
                                data: 'master',
                                csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
                            },
                            success: function(data) {
                                $('meta[name="csrf-token"]').attr("content", data
                                    .csrf_param);
                                console.log(data);
                                Datatables();
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                    )

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });

                    }
                    if (result.isDenied) {
                        $.ajax({
                            url: base_url + 'disnak/data-lampres/delete',
                            type: 'POST',
                            data: {
                                id: id,
                                data: 'transaksi',
                                csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
                            },
                            success: function(data) {
                                console.log(data);
                                $('meta[name="csrf-token"]').attr("content", data
                                    .csrf_param);
                                Datatables();
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                    )

                            },
                            error: function(request, textStatus, errorThrown) {
                                console.log(request.responseText);
                            }
                        });
                    }
                })

            });


            $.extend($.validator.prototype, {
                checkForm: function() {
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[
                        i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(
                            elements[i].name).length > 1) {
                            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                }
            });

            $("#form-lampres").validate({
                rules: {
                    "tahun": "required",
                    "bulan": "required",
                    "kd_kecamatan": "required",
                    "populasi[]": {
                        required: false
                    },
                    "pemotongan_ternak[]": {
                        required: false
                    },
                    "pemasukan[]": {
                        required: false
                    },
                    "pengeluaran[]": {
                        required: false
                    },
                    "kematian[]": {
                        required: false
                    },
                    "kelahiran[]": {
                        required: false
                    },

                },
                messages: {
                    "tahun": "Pilih Salah Satu",
                    "bulan": "Pilih Salah Satu",
                    "kd_kecamatan": "Pilih Salah Satu",

                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: Saved,
            });

            function Saved() {
                let FrmData = $('form#form-lampres').serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]')
                .attr("content");
                $.ajax({
                    url: $('#form-lampres').attr('action'),
                    type: 'post',
                    data: FrmData,
                    success: function(data) {
                        $('meta[name="csrf-token"]').attr("content", data.csrf_param);
                        $('#id_produksi').val('');
                        $('#add-modal').modal('hide');
                        Datatables();
                        Swal.fire({
                            position: 'centered',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });

            }
        });



function CheckingData() {
    let stats = false;
    let Data = $('#checked-data').closest('div').find("input,select,textarea");
    var data = {};
    $(Data.serializeArray()).each(function(index, obj) {
        data[obj.name] = obj.value;
        if (obj.name == 'tahun' && obj.value == '') {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Tahun Tidak Boleh Kosong',
            })
        }
        if (obj.name == 'bulan' && obj.value == '') {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Bulan Tidak Boleh Kosong',
            }).then((result) => {
                if (result.isConfirmed) {
                    var OpsKec = new Option('-Pilih Kecamatan-', '', true, true);
                    $('#kd_kecamatan').append(OpsKec);

                }

            })
        }
        if (obj.name != '' && obj.value != '') {
            stats = (obj.name == 'bulan' ? false : true);
        }

    });


    if (stats) {
        $.ajax({
            url: base_url + 'disnak/data-lampres/check-data',
            type: 'post',
            data: Data.serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content"),
            success: function(data) {
                $('meta[name="csrf-token"]').attr("content", data.csrf_param);
                if (data.status == 1) {
                    Swal.fire({
                        title: 'Ooops..',
                        html: data.msg,
                        icon: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Mengerti'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            var OpsKec = new Option('-Pilih Kecamatan-', '', true, true);
                            var OpsBln = new Option('==Pilih Bulan==', '', true, true);
                            var OpsThn = new Option('==Pilih Tahun==', '', true, true);
                            $('#bulan').append(OpsBln);
                            $('#tahun').append(OpsThn);
                            $('#kd_kecamatan').append(OpsKec);
                            $('tbody input').attr('readonly', true);
                            $('.prods').attr('readonly', true);

                        }

                    })
                }
                if (data.status == 2) {
                    $('form#form-lampres').attr('action', base_url + 'disnak/data-lampres/save/' + data
                        .data);
                    $('tbody input').attr('readonly', false);
                    $('.prods').attr('readonly', true);
                }
                if (data.status == 0) {
                    $('tbody input').attr('readonly', false);
                    $('.prods').attr('readonly', true);

                }
            }
        })

    }

}

function HasilProd(e, faktorX, id) {
    let Input = parseInt(e.value.replace(/\./g, '')) || 0;
    let Prods = parseInt(faktorX * Input);
    $('#prods_' + id).val(Prods);
    $('#prods_' + id).number(true, 0, ',', '.');
}

function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}
</script>
