<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends MY_Controller
{

	var $data = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
	}

	public function index()
	{
	}

	public function OptionSelect2()
	{
		$this->db->like('nama_kecamatan', $this->input->get('search'), 'BOTH');
		$Data = $this->db->get('tb_kecamatan')->result();
		foreach ($Data as $key => $value) {
			if ($value->kd_kecamatan != 0) {
				$Res[] = [
					'id' => $value->kd_kecamatan,
					'text' => $value->nama_kecamatan
				];
			}
		}
		$NewRes = [
			'id' => ' ',
			'text' => '==Pilih kecamatan=='
		];
		array_unshift($Res, $NewRes);
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
}

/* End of file Controllername.php */
