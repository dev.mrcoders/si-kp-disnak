<?php
Route::filter('logged_in', function () {

	if ($this->session->userdata('login') == FALSE) {
		$this->session->set_flashdata('session', 'Session Sudah Habis Silahkan Login');
		redirect(base_url());
	}
}); 

Route::get('login', 'login');
Route::post('login-checking', 'login/CheckLogin');
Route::get('islogout', 			'login/islogout');
Route::get('dashboard', 'dashboard', array('before' => 'logged_in'));
Route::get('select2-kecamatan', 'regions/kecamatan/OptionSelect2', array('before' => 'logged_in'));

Route::get('profile', 'account', array('before' => 'logged_in'));
Route::post('profile-update', 'account/UpdateProfile', array('before' => 'logged_in'));
Route::post('profile-update-password', 'account/UpdatePassword', array('before' => 'logged_in'));


Route::prefix('disnak', function () {
	Route::get('data-lampres', 'disnak/lampres/index', array());
	Route::prefix('data-lampres', function () {
		Route::get('index/(:any)', 'disnak/lampres/index/$1', array());
		Route::post('tables', 'disnak/lampres/DataTables', array('before' => 'logged_in'));
		Route::post('tables-form', 'disnak/lampres/DataTablesForm', array('before' => 'logged_in'));
		Route::match(['post','get'],'tables-report', 'disnak/lampres/datatablesreport', array('before' => 'logged_in'));
		Route::get('databyid', 'disnak/lampres/databyid', array('before' => 'logged_in'));
		Route::post('check-data', 'disnak/lampres/checkingdata', array('before' => 'logged_in'));
		Route::post('save', 'disnak/lampres/save', array('before' => 'logged_in'));
		Route::post('save/(:num)', 'disnak/lampres/save/$1', array('before' => 'logged_in'));
		Route::post('update/(:num)', 'disnak/lampres/update/$1', array('before' => 'logged_in'));
		Route::post('delete', 'disnak/lampres/delete', array('before' => 'logged_in'));
	});

	Route::prefix('data-ternak', function () {
		Route::get('index', 'disnak/komoditi', array('before' => 'logged_in'));
		Route::post('tables', 'disnak/komoditi/datatables', array('before' => 'logged_in'));
		Route::get('databyid', 'disnak/komoditi/databyid', array('before' => 'logged_in'));
		Route::post('save', 'disnak/komoditi/save', array('before' => 'logged_in'));
		Route::post('save/(:num)', 'disnak/komoditi/save/$1', array('before' => 'logged_in'));
		Route::post('delete', 'disnak/komoditi/delete', array('before' => 'logged_in'));
	});
});


