
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap"><i class="mdi mdi-navigation"></i> <span class="hide-menu">Navigation</span></li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}dashboard" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Dashboard</span></a>
                </li>
					@if(is_subdomain() == 'dispan')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}dispan/data-produksi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Produksi</span></a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="data-komoditi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Komoditi</span></a>
                    </li>
					@endif
					@if(is_subdomain() == 'disnak')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}disnak/data-lampres" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Lampres</span></a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}disnak/data-ternak/index" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Komoditi Ternak</span></a>
                    </li>
					@endif

					@if(is_subdomain() == 'diskan')
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Data Produksi</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="{{ base_url('diskan/') }}produksi-budidaya" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Budidaya Ikan</span></a></li>
                            <li class="sidebar-item"><a href="produksi-benih" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Benih Ikan</span></a></li>
                            <li class="sidebar-item"><a href="produksi-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Olahan Ikan</span></a></li>
                            <li class="sidebar-item"><a href="produksi-tangkapan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Tangkapan Ikan</span></a></li>
                        </ul>
                    </li>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-format-list-bulleted-type"></i><span class="hide-menu">Data Jenis</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="jenis-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Ikan</span></a></li>
                            <li class="sidebar-item"><a href="jenis-kegiatan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Kegiatan</span></a></li>
                            <li class="sidebar-item"><a href="jenis-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Produk Olahan</span></a></li>
                        </ul>
                    </li>
                @endif


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
