<footer id="footer" class="footer" style="margin-left: 0;">
	<div class="copyright">
		&copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
	</div>
	<div class="credits">
		Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
	</div>
</footer>