<!-- Bootstrap tether Core JavaScript -->
<script src="@asset('assets/')assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="@asset('assets/')assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@if ($classes != 'login')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js"
        integrity="sha512-42PE0rd+wZ2hNXftlM78BSehIGzezNeQuzihiBCvUEB3CVxHvsShF86wBWwQORNxNINlBPuq7rG4WWhNiTVHFg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"
        integrity="sha512-LRkOtikKE2LFHPWiWh0/bfFynswxRwCZ5O7PkXTVFPcprw376xfOemiEHEOmCCmiwS6eLFUh2fb+Gqxc0waTSg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- apps -->
    <script src="@asset('assets/')dist/js/app.min.js"></script>
    <script src="@asset('assets/')dist/js/app.init.mini-sidebar.js"></script>
    <script src="@asset('assets/')dist/js/app-style-switcher.js"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="@asset('assets/')assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="@asset('assets/')assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="@asset('assets/')dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="@asset('assets/')dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="@asset('assets/')dist/js/custom.js"></script>

    <script src="@asset('assets/')assets/extra-libs/c3/d3.min.js"></script>
    <script src="@asset('assets/')assets/extra-libs/c3/c3.min.js"></script>
    <script src="@asset('assets/')assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="@asset('assets/')assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>


    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.3.0/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.3.1/js/dataTables.fixedHeader.min.js"></script>
    <script src="@asset('assets/')assets/libs/toastr/build/toastr.min.js"></script>

    <script src="@asset('assets/')assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="@asset('assets/')assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="@asset('assets/')assets/libs/jquery.repeater/jquery.repeater.min.js"></script>
    <script src="@asset('assets/')assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="@asset('assets/')assets/js/jquery.number.js"></script>
    <script src="@asset('assets/')assets/js/jquery.session.js"></script>

    <script>
        function Success(msg = '') {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: (msg != '' ? msg : 'Your work has been saved'),
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
@endif
<script>
    var csrfname = '{{ get_csrf_token_name() }}';
    var csrfhash = '{{ get_csrf_hash() }}';
    $('meta[name="csrf-param"]').attr("content", csrfname);
    $('meta[name="csrf-token"]').attr("content", csrfhash);
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    const base_url = '{{ base_url('') }}';

    function ajaxcsrf() {
        var csrf = {};
        csrf[csrfname] = csrfhash;
        $.ajaxSetup({
            "data": csrf
        });
    }

    function update_csrf_fields(value) {
        $('[name="csrf_hash_name"]').val(value);
    }
</script>



@if (flashdata(false, 'session', 'session habis'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: '{{ flashdata(false, 'session') }}',
        })
    </script>
@endif
